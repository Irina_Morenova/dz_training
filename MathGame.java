import java.util.InputMismatchException;
import java.util.Scanner;

public class MathGame{
    Scanner sin;
    String name;

    public MathGame(){
        sin = new Scanner(System.in);

        System.out.print("What is your name?");
        name = sin.next();
        System.out.println("Hello " + name + "!");

        dialog();
    }

    private void dialog(){
        while (true){
            System.out.println(name + ", are you ready to start the game? (Y/N)");
            if (sin.next().equalsIgnoreCase("Y")){
                System.out.println("Perform arithmetic calculations");
                math();
            } else {
                System.out.println("Bye");
                System.exit(0);
            }
        }
    }

    private void math(){
        final int STEP = 10;

        int a, b, answer;
        int countYes = 0;

        for (int i = 0; i < STEP; i++){
            a = numberGenerate();
            b = numberGenerate();
            System.out.printf("%d + %d = ", a, b);
            try{
                answer = sin.nextInt();
            } catch (InputMismatchException e){
                System.out.println("The answer must contain only numbers.");
                sin.next();
                continue;
            }

            if (equals(calculation(a, b), answer)){
                System.out.println("Yes!");
                countYes++;
            } else {
                System.out.println("Oh, no...");
            }
        }
        System.out.printf("Right answers %d wrong %d\n", countYes, STEP - countYes);

    }

    private boolean equals(int a, int b){
        return a == b;
    }

    private int calculation(int a, int b){
        return a + b;
    }

    private int numberGenerate(){
        return (int) (100 * Math.random());
    }

    public static void main(String[] args){
        new MathGame();
    }
}